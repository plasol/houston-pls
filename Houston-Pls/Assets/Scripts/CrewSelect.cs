﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CrewSelect : MonoBehaviour
{
    public GameManager gameManager;
    public GraphicManager graphicManager;

    public Image charImage;
    public TextMeshProUGUI descriptionText;

    public Image iconImage;
    public Sprite[] icons;

    public Image member1;
    public Image member2;
    public Image member3;

    public Text titleText;
    public GameObject startButton;

    public int showCharIndex = 0;
    public List<int> crewCount;
    //int crewCount = 0;

    List<string> crewDescription = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        crewCount.Add(5);

        crewDescription.Add("Name: Sarah.\n\nAttribute: Chemist.\n\nI am a Chemist from Istanbul, Turkey. If you miss me, you can call me on Avogadro’s number!");
        crewDescription.Add("Name: Paolo.\n\nAttribute: DJ / Musician..that’s out of this World”.\n\n“YO! I am a skilled DJ from the beautiful city of Rio de Janeiro, Brazil! I am very resourceful & I like to cheer people up.I want to be the first to play “ music.");
        crewDescription.Add("Name: Kana.\n\nAttribute: Writer.\n\nI studied Political Science with a minor in communication.I hope to use my skills to help solve conflicts that may generate during the mission.");
        crewDescription.Add("Name: Fiona.\n\nAttribute: Physicist.\n\nI am an expert of physics and space machines.");      
        crewDescription.Add("Name: Luke.\n\nAttribute: Space engineer.\n\nAfter many years of technical studies and hard Nasa training at least i can go to space!.");
        crewDescription.Add("");  
        crewDescription.Add("Name: Hanna.\n\nAttribute: Doctor.\n\n“I have graduated in Biomedical Engineering from the University of Munchen, Germany.After my NASA training I consider myself a “Space doctor”");
        crewDescription.Add("Name: Diletta.\n\nAttribute: RollingFolks Representative.\n\n“Hello Evryone from Italy! Hope you're enjoing this demo. See you in space!”");

        startButton.SetActive(false); 
    }

    // Update is called once per frame
    void Update()
    {
        charImage.sprite = graphicManager.charactersSprite[showCharIndex];
        if (crewDescription.Count > showCharIndex) descriptionText.text = crewDescription[showCharIndex];
    }

    public void slideRight()
    {
        showCharIndex++;
        showCharIndex = showCharIndex % graphicManager.charactersSprite.Length;
        if (crewCount.Contains(showCharIndex))
            if (crewCount.Count < graphicManager.charactersSprite.Length)
                slideRight();
    }

    public void slideLeft()
    {
        showCharIndex--;
        if (showCharIndex < 0) showCharIndex = graphicManager.charactersSprite.Length - 1;
        if (crewCount.Contains(showCharIndex))
            if (crewCount.Count < graphicManager.charactersSprite.Length)
                slideLeft();
    }

    public void addAstronaut()
    {
        if (crewCount.Count < 4)
        {

            switch (crewCount.Count)
            {
                case 1:
                    member1.sprite = graphicManager.charactersSprite[showCharIndex];
                    break;
                case 2:
                    member2.sprite = graphicManager.charactersSprite[showCharIndex];
                    break;
                case 3:
                    member3.sprite = graphicManager.charactersSprite[showCharIndex];
                    break;
            }

            crewCount.Add(showCharIndex);
            slideRight();        
        }

        if (crewCount.Count >= 4)
        {
            startButton.SetActive(true);
            titleText.text = "LET'S TAKE OFF!";
        }  
    }
}
