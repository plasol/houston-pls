﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GraphicManager graphicManager;
    public SoundManager soundManager;

    public GameObject startScene;
    public GameObject editScene;
    public GameObject tutorialScene;
    public GameObject gameScene;
    public GameObject endingScene;

    public GameObject answerA;
    public GameObject answerB;
    public GameObject answerC;

    public Image doubleTimeMask;

    public Image mainScreen;
    public Image captainImage;
    public TextMeshProUGUI conversationText;

    public GameObject[] logObject;

    public Text timerText;

    public GameObject coffe;

    public int tempSelectedLevelIndex;      // brutto
    public int selectedLevelIndex;

    Queue<Evento> eventQueue = new Queue<Evento>();
    Queue<float> eventCountdownQueue = new Queue<float>();

    public List<Evento> dispachedEvents = new List<Evento>();

    public Evento selectedEvent = null;
    public int selectedEventTextIndex = 0;

    public const int MAX_PARAMETER_VALUE = 30;
    public GameState gameState = new GameState();

    public float timer = 0;
    public bool isTimeFlowing = false;

    public float nextDispatch;

    //public bool mustResetScreen = false;
    public float resetScreen = 4;
    public float waitScreenSaver = 3;

    public bool doubleTime = false;
    public bool halfTime = false;

    public bool isTutorialAppeared = false;

    // Start is called before the first frame update
    void Start()
    {
        graphicManager.setLogObj(logObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimeFlowing)
        {
            if (timer > 0)
            {
                if (isLosing())
                {
                    loseLevel();
                }

                if (doubleTime)
                {
                    timer -= Time.deltaTime * 2;
                    nextDispatch -= Time.deltaTime * 2;
                }
                else if (halfTime)
                {
                    timer -= Time.deltaTime / 2;
                    nextDispatch -= Time.deltaTime / 2;
                }
                else
                {
                    timer -= Time.deltaTime;
                    nextDispatch -= Time.deltaTime;
                } 

                string decimalPart = ((int)((timer - (int)timer)*100)).ToString();
                if (decimalPart.Length == 1) decimalPart += "0";
                timerText.text = "TIME: " + ((int)timer).ToString() + ":" + decimalPart;
                if (nextDispatch <= 0)
                {
                    Dispatch();
                    if (eventCountdownQueue.Count > 0)
                    {
                        nextDispatch = eventCountdownQueue.Dequeue();
                    }
                }

                // reset screen
                if (selectedEvent == null)
                {
                    if (resetScreen > 0) resetScreen -= Time.deltaTime;
                    else
                    {
                        captainImage.sprite = graphicManager.captainExpression[0];
                        mainScreen.sprite = graphicManager.conversationBg[3];
                        conversationText.transform.parent.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                Debug.Log("Time run out!");
                endLevel();
            }
        }
    }

    public void loadScene(GameObject scene)
    {
        startScene.SetActive(false);
        editScene.SetActive(false);
        tutorialScene.SetActive(false);
        gameScene.SetActive(false);
        endingScene.SetActive(false);

        scene.SetActive(true);

        if (scene = startScene) soundManager.PlayOST(0);

        // if (scene = game scene....
    }

    // carico la crew select
    public void loadCrewSelect(int levelIndex)
    {
        loadScene(editScene);
    }

    // orribile
    public void loadLevelFromCrewSelect()
    {
        loadLevel(tempSelectedLevelIndex);
    }

    // carico il tutorial
    public void loadTutorial()
    {
        if (!isTutorialAppeared) loadScene(tutorialScene);
        else loadLevelFromCrewSelect();
    }

    // carico un livello
    public void loadLevel(int levelIndex)
    {
        selectedLevelIndex = levelIndex;
        loadScene(gameScene);

        Level level;
        switch (levelIndex)
        {
            case 2:
                level = GameData.secondLevel;
                soundManager.PlayOST(2);
                break;
            case 1:
            default:
                level = GameData.firstLevel;
                soundManager.PlayOST(1);
                break;
        }

        timer = level.timer;
        foreach(var evento in level.events)
        {
            eventQueue.Enqueue(evento);
        }

        foreach (var countdown in level.nextEvent)
        {
            eventCountdownQueue.Enqueue(countdown);
        }

        // carico il primo dispatch
        nextDispatch = eventCountdownQueue.Dequeue();

        startGame();
    }

    public void retryLevel()
    {
        loadScene(gameScene);
        loadLevel(selectedLevelIndex);
        startGame();
    }

    // avvio il gioco
    public void startGame()
    {
        gameState = new GameState() 
        { 
            blood = MAX_PARAMETER_VALUE, 
            sight = MAX_PARAMETER_VALUE, 
            radiation = MAX_PARAMETER_VALUE, 
            psych = MAX_PARAMETER_VALUE, 
            muscle = MAX_PARAMETER_VALUE, 
            coordination = MAX_PARAMETER_VALUE
        };
        graphicManager.ShowParameters(gameState);
        dispachedEvents = new List<Evento>();

        graphicManager.showDefault();
        graphicManager.ShowLog(dispachedEvents);
        isTimeFlowing = true;

        selectedEvent = null;

        captainImage.sprite = graphicManager.captainExpression[0];
        mainScreen.sprite = graphicManager.conversationBg[3];
        conversationText.transform.parent.gameObject.SetActive(false);

        selectedEventTextIndex = 0;
        doubleTime = false;
        halfTime = false;
    }

    //fine livello
    public void endLevel()
    {
        isTimeFlowing = false;

        foreach(var evento in dispachedEvents)
        {
            missEvent(evento);
        }
        if (isLosing()) graphicManager.ShowLosingScreen();
        else if (selectedLevelIndex == 2) graphicManager.ShowVictoryScreen();
        else graphicManager.ShowNextLevelScreen();
        loadScene(endingScene);
    }

    public void doubleTimeActive()
    {
        halfTime = false;
        doubleTime = !doubleTime;
        if (doubleTime) doubleTimeMask.color = new Color(0, 0, 1, 0.4f);
        else doubleTimeMask.color = new Color(0, 0, 1, 0); ;
    }

    public void coffeActive()
    {
        doubleTime = false;
        halfTime = !halfTime;
    }

    // mando l'evento sul log
    public void Dispatch()
    {
        if (eventCountdownQueue.Count == 0) return;
        dispachedEvents.Add(eventQueue.Dequeue());

        // se il log è pieno
        if (dispachedEvents.Count > logObject.Length)
        {
            // misso il primo evento
            missEvent(dispachedEvents[0]);
            dispachedEvents.RemoveAt(0);
        }

        graphicManager.ShowLog(dispachedEvents);
        soundManager.ButtonTutituEffect();
    }

    // seleziono l'evento dal log
    public void runEvent(int logIndex)
    {
        if (logIndex < dispachedEvents.Count)
        {
            if (selectedEvent == null)
            {
                selectedEvent = dispachedEvents[logIndex];
                dispachedEvents.RemoveAt(logIndex);
                showEvent();

                graphicManager.ShowLog(dispachedEvents);

            }
        }
    }

    // mostro l'evento selezionato
    public void showEvent()
    {
        selectedEventTextIndex = -1;
        showNextDialog();

        // imposto il giusto bg
        if (graphicManager.conversationBg.Length > selectedEvent.imageId)
            mainScreen.sprite = graphicManager.conversationBg[selectedEvent.imageId];

        // capitano
        if (graphicManager.captainExpression.Length > selectedEvent.captainId)
            captainImage.sprite = graphicManager.captainExpression[selectedEvent.captainId];

        // container testo
        conversationText.transform.parent.gameObject.SetActive(true);

    }

    public void showNextDialog()
    {
        if (selectedEvent == null) return;

        if (selectedEvent.content.Count > selectedEventTextIndex + 1) selectedEventTextIndex += 1;
        if (selectedEvent.content.Count == selectedEventTextIndex + 1)
        {
            conversationText.text =
                selectedEvent.content[selectedEventTextIndex] +
                "\n\n<b>A</b>. " + selectedEvent.answers[0].content +
                "\n<b>B</b>. " + selectedEvent.answers[1].content +
                "\n<b>C</b>. " + selectedEvent.answers[2].content;
        }
        else
        {
            conversationText.text = selectedEvent.content[selectedEventTextIndex];
        }
    }

    public void slectAnswer(int answerIndex)
    {
        if (selectedEvent == null) return;

        if (selectedEvent.content.Count == selectedEventTextIndex + 1)
        {
            if (selectedEvent.answers.Length > answerIndex)
            {
                gameState.parameterVariation(selectedEvent.answers[answerIndex].parameterVariation);
                conversationText.text = selectedEvent.answers[answerIndex].feedback;

                // feedback
                if (graphicManager.captainExpression.Length > selectedEvent.answers[answerIndex].captainId)
                    captainImage.sprite = graphicManager.captainExpression[selectedEvent.answers[answerIndex].captainId];

                selectedEvent = null;
                graphicManager.ShowParameters(gameState);

                // screen reset
                resetScreen = 4;
            }
        }
    }

    public void missEvent(Evento evento)
    {
        var missState = new GameState() { blood = -10, sight = -10, radiation = -10, psych = -10, muscle = -10, coordination = -10 };
        gameState.parameterVariation(missState);
        graphicManager.ShowParameters(gameState);

        // alert
        conversationText.text = "We are waiting for ages!";
        captainImage.sprite = graphicManager.captainExpression[4];
    }

    // se due parametri arrivano a zero hai perso
    private bool isLosing()
    {
        int count = 0;
        if (gameState.blood <= 0) count++;
        if (gameState.coordination <= 0) count++;
        if (gameState.muscle <= 0) count++;
        if (gameState.psych <= 0) count++;
        if (gameState.radiation <= 0) count++;
        if (gameState.sight <= 0) count++;

        if (count >= 2) return true;
        else return false;
    }

    public void loseLevel()
    {
        isTimeFlowing = false;
        graphicManager.ShowLosingScreen();
        loadScene(endingScene);
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
