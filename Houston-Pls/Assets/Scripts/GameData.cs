﻿/*
    {
        radiation = +-n,
        muscle = +-n,
        blood = +-n,
        sight = +-n,
        psych = +-n,
        coordination = +-n,
    }
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{

    // first level (easy)
        // first event
            public static Evento.Answer l1_firstEventAnswer1 = new Evento.Answer() { content = "Make them sit in a horizontal position!", captainId = 1, parameterVariation = new GameState() {coordination = -5, blood = -5}, feedback = "Copy & out!" };
            public static Evento.Answer l1_firstEventAnswer2 = new Evento.Answer() { content = "Can you make them lay on their side?", captainId = 5, parameterVariation = new GameState() {coordination = -15, blood = -5}, feedback = "I am not convinced, but we can try…" };
            public static Evento.Answer l1_firstEventAnswer3 = new Evento.Answer() { content = "Give them a painkiller for the time being, they'll feel better in no time!", captainId = 2, parameterVariation = new GameState() {coordination = -10, blood = -5 }, feedback = "I don’t think that will change anything…" };
    public static Evento l1_firstEvent = new Evento() { logText = "Strong acceleration during launch!", captainId = 1, imageId = 2, content = new List<string>() { "Houston, some people are feeling dizzy and are having problem with coordination during the lauch", "What should we do?" }, answers = new Evento.Answer[] { l1_firstEventAnswer1, l1_firstEventAnswer2, l1_firstEventAnswer3 } } ;
        // second event
            public static Evento.Answer l1_secondEventAnswer1 = new Evento.Answer() { content = "Go as fast as you can but care to keep a straight direction!", captainId = 2, parameterVariation = new GameState() {radiation = -10, coordination = -10}, feedback = "I don't thing going faster might help..." };
            public static Evento.Answer l1_secondEventAnswer2 = new Evento.Answer() { content = "They are unavoidable, pick the less radiative spot and procede", captainId = 1, parameterVariation = new GameState() {radiation = -10}, feedback = "I hear you" };
            public static Evento.Answer l1_secondEventAnswer3 = new Evento.Answer() { content = "There is an area without radiation but filled with small debris..", captainId = 5, parameterVariation = new GameState() {psych = -5}, feedback = "I will inform you of the results" };
            public static Evento l1_secondEvent = new Evento() {logText = "Crossing Van Allen band", captainId = 1, imageId = 8, content = new List<string>() { "We are crossing the Van Allen bands and our devices are detecting lots of incoming radiation!", "Can we protect ourselves from this?" }, answers = new Evento.Answer[] { l1_secondEventAnswer1, l1_secondEventAnswer2, l1_secondEventAnswer3 } } ;
        // third event
            public static Evento.Answer l1_thirdEventAnswer1 = new Evento.Answer() { content = "There is also another part in sector G6 to repair... Let's work on that as well", captainId = 4, parameterVariation = new GameState() {sight = -10}, feedback = "Uff..." };
            public static Evento.Answer l1_thirdEventAnswer2 = new Evento.Answer() { content = "It's worse if we don't addres this, take a coffee and we'll be in touch", captainId = 5, parameterVariation = new GameState() {sight = -5, psych = -5}, feedback = "Let's try..." };
            public static Evento.Answer l1_thirdEventAnswer3 = new Evento.Answer() { content = "Don't worry and take a nap, our engineers are already working on this", captainId = 1, parameterVariation = new GameState() {coordination = +3}, feedback = "Thank you Houston" };
            public static Evento l1_thirdEvent = new Evento() {logText = "Equipment damaged", captainId = 1, imageId = 10, content = new List<string>() { "One of the on board equipment has been damaged!", "We are really exhausted and stressed, everything we tried didn't work and have made us tired" }, answers = new Evento.Answer[] { l1_thirdEventAnswer1, l1_thirdEventAnswer2, l1_thirdEventAnswer3 } } ;
        // fourth event
            public static Evento.Answer l1_fourthEventAnswer1 = new Evento.Answer() { content = "Make them have a drink and calm down", captainId = 5, parameterVariation = new GameState() {psych = -5, sight = +3}, feedback = "That'll do" };
            public static Evento.Answer l1_fourthEventAnswer2 = new Evento.Answer() { content = "Good call, make them talk with a psychologist", captainId = 1, parameterVariation = new GameState() {psych = +5}, feedback = "It's probably the best solution possible" };
            public static Evento.Answer l1_fourthEventAnswer3 = new Evento.Answer() { content = "Take a nap, this'll solve itself", captainId = 2, parameterVariation = new GameState() {psych = -5, coordination = +3}, feedback = "No effects detected" };
            public static Evento l1_fourthEvent = new Evento() {logText = "Fight with crewmate", captainId = 1, imageId = 6, content = new List<string>() { "Houston, two of my team have been fighting", "now everything is under control but I don't want to run any risk in the future, what should I do?" }, answers = new Evento.Answer[] { l1_fourthEventAnswer1, l1_fourthEventAnswer2, l1_fourthEventAnswer3 } } ;
        // fifth event
            public static Evento.Answer l1_fifthEventAnswer1 = new Evento.Answer() { content = "It might be! Do some physiotherapy for your body", captainId = 1, parameterVariation = new GameState() {muscle = +3, psych = +3}, feedback = "Thanks for the support" };
            public static Evento.Answer l1_fifthEventAnswer2 = new Evento.Answer() { content = "Just go to sleep, don't worry tomorrow this problem is going to be solved", captainId = 4, parameterVariation = new GameState() {muscle = -15, psych = +3}, feedback = "... But the crew is still in a bad state!" };
            public static Evento.Answer l1_fifthEventAnswer3 = new Evento.Answer() { content = "You risk some atrophy to your muscle, do a workout immediately!", captainId = 1, parameterVariation = new GameState() {psych = +3, muscle = -5}, feedback = "Excellent choice!" };
            public static Evento l1_fifthEvent = new Evento() {logText = "Sharing same space for a long time", captainId = 1, imageId = 0, content = new List<string>() {"There has been an issue with a room and it has been shut down until repairement are completed", "We've been living in a small space for days now, is this a problem?" }, answers = new Evento.Answer[] { l1_fifthEventAnswer1, l1_fifthEventAnswer2, l1_fifthEventAnswer3 } } ;
        // sixth event
            public static Evento.Answer l1_sixthEventAnswer1 = new Evento.Answer() { content = "Don't panic, reach the safety equipment, we'll handle the maneuver", captainId = 1, parameterVariation = new GameState() {muscle = -5}, feedback = "You've saved us!" };
            public static Evento.Answer l1_sixthEventAnswer2 = new Evento.Answer() { content = "Relax, have a coffee", captainId = 4, parameterVariation = new GameState() {coordination = -15}, feedback = "Oh, I see... We're all gonna DIE" };
            public static Evento.Answer l1_sixthEventAnswer3 = new Evento.Answer() { content = "Go to the shielded room", captainId = 5, parameterVariation = new GameState() {coordination = -5, muscle = -5}, feedback = "That will not do the job for sure!" };
            public static Evento l1_sixthEvent = new Evento() {logText = "Ordinary maneuver needed", captainId = 1, imageId = 5, content = new List<string>() { "Houston, we might have a problem", "We've detected some small asteroids ahead of us, PLS help!" }, answers = new Evento.Answer[] { l1_sixthEventAnswer1, l1_sixthEventAnswer2, l1_sixthEventAnswer3 } } ;
        // seventh event
            public static Evento.Answer l1_seventhEventAnswer1 = new Evento.Answer() { content = "Do some excersize, it'll go away", captainId = 3, parameterVariation = new GameState() {blood = -5, muscle = +3}, feedback = "Your solution is not working" };
            public static Evento.Answer l1_seventhEventAnswer2 = new Evento.Answer() { content = "You probably have an accumulation of blood, don't worry, use the decopmressurizant equipment!", captainId = 1, parameterVariation = new GameState() {blood = +5}, feedback = "That really improved my health!" };
            public static Evento.Answer l1_seventhEventAnswer3 = new Evento.Answer() { content = "Assume some anti-thrombosis drugs and it'll pass", captainId = 5, parameterVariation = new GameState() {psych = -5}, feedback = "Hmmm..." };
            public static Evento l1_seventhEvent = new Evento() {logText = "Thrombosis risk", captainId = 1, imageId = 7, content = new List<string>() { "I've been feeling strange in my legs", "They are hurting, what is happening to me?" }, answers = new Evento.Answer[] { l1_seventhEventAnswer1, l1_seventhEventAnswer2, l1_seventhEventAnswer3 } } ;
        // eight event
            public static Evento.Answer l1_eightEventAnswer1 = new Evento.Answer() { content = "Make him talk with a psychologist", captainId = 1, parameterVariation = new GameState() {psych = +7}, feedback = "That will do the job" };
            public static Evento.Answer l1_eightEventAnswer2 = new Evento.Answer() { content = "Let him relax with some music, spread his other tasks to the crew for today", captainId = 1, parameterVariation = new GameState() {psych = -5, coordination = +3}, feedback = "No problem with that" };
            public static Evento.Answer l1_eightEventAnswer3 = new Evento.Answer() { content = "Let him have a hot shower", captainId = 2, parameterVariation = new GameState() {psych = -5, sight = +3}, feedback = "Maybe so..." };
            public static Evento l1_eightEvent = new Evento() {logText = "Overstress in the crew", captainId = 1, imageId = 0, content = new List<string>() { "One of my guys have had a stress crysis", "What should we do?" }, answers = new Evento.Answer[] { l1_eightEventAnswer1, l1_eightEventAnswer2, l1_eightEventAnswer3 } } ;

    // second level (hard)
        // first event
            public static Evento.Answer l2_firstEventAnswer1 = new Evento.Answer() { content = "Go to sleep, tomorrow we'll fix this", captainId = 4, parameterVariation = new GameState() {muscle = -15, psych = +3}, feedback = "I can't take this anymore..." };
            public static Evento.Answer l2_firstEventAnswer2 = new Evento.Answer() { content = "Care for muscle atrophy, take turns in the gym", captainId = 2, parameterVariation = new GameState() {muscle = -5, psych = +3}, feedback = "You could be right" };
            public static Evento.Answer l2_firstEventAnswer3 = new Evento.Answer() { content = "Do some physiotherapy for your body, that'll do", captainId = 1, parameterVariation = new GameState() {muscle = +3, psych = +3}, feedback = "You really got it Houston!" };
            public static Evento l2_firstEvent = new Evento() {logText = "Sharing same space for a long time", captainId = 1, imageId = 0, content = new List<string>() { "It happened again, we are constricted in the same room, suggestion?" }, answers = new Evento.Answer[] { l2_firstEventAnswer1, l2_firstEventAnswer2, l2_firstEventAnswer3 } } ;
        // second event
            public static Evento.Answer l2_secondEventAnswer1 = new Evento.Answer() { content = "Go faster but keep the direction straight", captainId = 3, parameterVariation = new GameState() {radiation = -10, coordination = -5}, feedback = "I have a bad feeling about this..." };
            public static Evento.Answer l2_secondEventAnswer2 = new Evento.Answer() { content = "Move to the safe zone, it's shielded against radiation", captainId = 2, parameterVariation = new GameState() {radiation = -5}, feedback = "I hear what you are saying" };
            public static Evento.Answer l2_secondEventAnswer3 = new Evento.Answer() { content = "Don't worry about this, just have a coffee", captainId = 4, parameterVariation = new GameState() {radiation = -10, psych = -5}, feedback = "That’s just wrong…" };
            public static Evento l2_secondEvent = new Evento() {logText = "Maximum in solar cycle", captainId = 1, imageId = 8, content = new List<string>() { "We're measuring an unexpected amount of radiations hitting the shuttle", "Is this normal?" }, answers = new Evento.Answer[] { l2_secondEventAnswer1, l2_secondEventAnswer2, l2_secondEventAnswer3 } } ;
        // third event
            public static Evento.Answer l2_thirdEventAnswer1 = new Evento.Answer() { content = "Make him drink something hot", captainId = 3, parameterVariation = new GameState() {blood = -10, coordination = +3, psych = -5}, feedback = "Looks like a terrible decision" };
            public static Evento.Answer l2_thirdEventAnswer2 = new Evento.Answer() { content = "This might happen when you eat too much salt, change his diet, he'll be fine in the next days", captainId = 1, parameterVariation = new GameState() {blood = -5, muscle = +3}, feedback = "Thank you for the info" };
            public static Evento.Answer l2_thirdEventAnswer3 = new Evento.Answer() { content = "You should have some anti kidney stone drug, give him that", captainId = 1, parameterVariation = new GameState() {blood = +3, psych = -5}, feedback = "He's feeling good already, thank you!" };
            public static Evento l2_thirdEvent = new Evento() {logText = "Kidney stones", captainId = 1, imageId = 7, content = new List<string>() { "One of my mates is having kidney stones", "He's in pain what can I do?" }, answers = new Evento.Answer[] { l2_thirdEventAnswer1, l2_thirdEventAnswer2, l2_thirdEventAnswer3 } } ;
        // fourth event
            public static Evento.Answer l2_fourthEventAnswer1 = new Evento.Answer() { content = "Make him talk with a psychologist", captainId = 1, parameterVariation = new GameState() {psych = +7}, feedback = "That will do the job"  };
            public static Evento.Answer l2_fourthEventAnswer2 = new Evento.Answer() { content = "Let him have a hot shower", captainId = 2, parameterVariation = new GameState() {psych = -5, sight = +3}, feedback = "Maybe so..." };
            public static Evento.Answer l2_fourthEventAnswer3 = new Evento.Answer() { content = "Let him relax with some music, spread his other tasks to the crew for today", captainId = 1, parameterVariation = new GameState() {psych = -5, coordination = +3}, feedback = "No problem with that" };
            public static Evento l2_fourthEvent = new Evento() {logText = "Overstress in the crew", captainId = 1, imageId = 0, content = new List<string>() { "One of my guys have had a stress crysis", "What should we do?" }, answers = new Evento.Answer[] { l2_fourthEventAnswer1, l2_fourthEventAnswer2, l2_fourthEventAnswer3 } } ;
        // fifth event
            public static Evento.Answer l2_fifthEventAnswer1 = new Evento.Answer() { content = "Go to darken the window now, we'll postpone our activities for today", captainId = 1, parameterVariation = new GameState() {psych = -5}, feedback = "We are lucky to have you" };
            public static Evento.Answer l2_fifthEventAnswer2 = new Evento.Answer() { content = "You'll sleep better next time, get a coffe and off to work", captainId = 5, parameterVariation = new GameState() {psych = -5, sight = -5}, feedback = "Roger that" };
            public static Evento.Answer l2_fifthEventAnswer3 = new Evento.Answer() { content = "Don't worry we'll adjust your sleep schedule according to this", captainId = 4, parameterVariation = new GameState() {sight = -10, psych = -5}, feedback = "It looks like an horrible decision" };
            public static Evento l2_fifthEvent = new Evento() {logText = "Forgot to darken the window", captainId = 1, imageId = 9, content = new List<string>() { "We've forgot to darken the window for the night", "We have slept really poorly this night" }, answers = new Evento.Answer[] { l2_fifthEventAnswer1, l2_fifthEventAnswer2, l2_fifthEventAnswer3 } } ;
        // sixth event
            public static Evento.Answer l2_sixthEventAnswer1 = new Evento.Answer() { content = "Do some physical excersize", captainId = 1, parameterVariation = new GameState() {muscle = +3, coordination = +3}, feedback = "Nice, copy and out" };
            public static Evento.Answer l2_sixthEventAnswer2 = new Evento.Answer() { content = "We should have provided enough integrator for this, take some", captainId = 1, parameterVariation = new GameState() {blood = -5}, feedback = "Your solution is working" };
            public static Evento.Answer l2_sixthEventAnswer3 = new Evento.Answer() { content = "Go to sleep with the windows exposed to the sun", captainId = 3, parameterVariation = new GameState() {psych = -10, muscle = -10, sight = -10}, feedback = "That is just dumb..." };
            public static Evento l2_sixthEvent = new Evento() {logText = "Vitamin D deficiency", captainId = 1, imageId = 7, content = new List<string>() { "We are having some deficiency with vitamine D", "What can we take?" }, answers = new Evento.Answer[] { l2_sixthEventAnswer1, l2_sixthEventAnswer2, l2_sixthEventAnswer3 } } ;
        // seventh event
            public static Evento.Answer l2_seventhEventAnswer1 = new Evento.Answer() { content = "Don't worry about this, not a big problem", captainId = 4, parameterVariation = new GameState() {radiation = -15}, feedback = "It's getting worse now... Not really helpful" };
            public static Evento.Answer l2_seventhEventAnswer2 = new Evento.Answer() { content = "Go to the protected room, we'll first do a checkup on its state", captainId = 5, parameterVariation = new GameState() { radiation = -5, coordination = -5}, feedback = "Thanks for the answer" };
            public static Evento.Answer l2_seventhEventAnswer3 = new Evento.Answer() { content = "Repair the shiled, you have all the equipment needed for this, we'll walk you through this", captainId = 1, parameterVariation = new GameState() {sight = -5}, feedback = "Brilliant as always" };
            public static Evento l2_seventhEvent = new Evento() {logText = "Leak in radiation shield", captainId = 1, imageId = 4, content = new List<string>() { "There is a leak in the radiation protection system", "How do we solve this?" }, answers = new Evento.Answer[] { l2_seventhEventAnswer1, l2_seventhEventAnswer2, l2_seventhEventAnswer3 } } ;
        // eight event
            public static Evento.Answer l2_eightEventAnswer1 = new Evento.Answer() { content = "Relax, have a coffee", captainId = 4, parameterVariation = new GameState() {coordination = -15}, feedback = "Oh, I see... We're all gonna DIE" };
            public static Evento.Answer l2_eightEventAnswer2 = new Evento.Answer() { content = "Go to the shielded room", captainId = 2, parameterVariation = new GameState() {coordination = -5, muscle = -5}, feedback = "That will not do the job for sure!" };
            public static Evento.Answer l2_eightEventAnswer3 = new Evento.Answer() {  content = "Don't panic, reach the safety equipment, we'll handle the maneuver", captainId = 1, parameterVariation = new GameState() {muscle = -5}, feedback = "You've saved us!" };
            public static Evento l2_eightEvent = new Evento() {logText = "Ordinary maneuver needed", captainId = 1, imageId = 10, content = new List<string>() {"Houston, we might have a problem", "We've detected some small asteroids ahead of us, PLS help!" }, answers = new Evento.Answer[] { l2_eightEventAnswer1, l2_eightEventAnswer2, l2_eightEventAnswer3 } } ;
        // nineth event
            public static Evento.Answer l2_ninethEventAnswer1 = new Evento.Answer() {content = "You probably have an accumulation of blood, don't worry, use the decopmressurizant equipment!", captainId = 1, parameterVariation = new GameState() {blood = +5}, feedback = "That really improved my health!"};
            public static Evento.Answer l2_ninethEventAnswer2 = new Evento.Answer() { content = "Do some excersize, it'll go away", captainId = 2, parameterVariation = new GameState() {blood = -5, muscle = +3}, feedback = "Your solution is not working"  };
            public static Evento.Answer l2_ninethEventAnswer3 = new Evento.Answer() { content = "Assume some anti-thrombosis drugs and it'll pass", captainId = 3, parameterVariation = new GameState() {psych = -5}, feedback = "Hmmm..."  };
            public static Evento l2_ninethEvent = new Evento() {logText = "Thrombosis ", captainId = 1, imageId = 5, content = new List<string>() { "I've been feeling strange in my legs", "They are hurting, what is happening to me?"}, answers = new Evento.Answer[] { l2_ninethEventAnswer1, l2_ninethEventAnswer2, l2_ninethEventAnswer3 } } ;
        // tenth event
            public static Evento.Answer l2_tenthEventAnswer1 = new Evento.Answer() { content = "Move to the safe zone shielded from radiation", captainId = 1, parameterVariation = new GameState() {radiation = -5}, feedback = "Thanks for the feedback" };
            public static Evento.Answer l2_tenthEventAnswer2 = new Evento.Answer() { content = "Keep going, it will pass", captainId = 4, parameterVariation = new GameState() {radiation = -10, coordination = -5}, feedback = "It's worse now, you didn't really help us" };
            public static Evento.Answer l2_tenthEventAnswer3 = new Evento.Answer() { content = "Get a coffee, you're almost there", captainId = 4, parameterVariation = new GameState() {psych = -10, coordination = -10, radiation = -10}, feedback = "I think we won't survive this one" };
            public static Evento l2_tenthEvent = new Evento() {logText = "Solar wind", captainId = 1, imageId = 8, content = new List<string>() { "He've measured a peak in solar radiation hitting the shuttle", "What can we do?" }, answers = new Evento.Answer[] { l2_tenthEventAnswer1, l2_tenthEventAnswer2, l2_tenthEventAnswer3 } } ;


    // levels
        public static Level firstLevel = new Level() { events = new List<Evento>() { l1_firstEvent, l1_secondEvent, l1_thirdEvent, l1_fourthEvent, l1_fifthEvent, l1_sixthEvent, l1_seventhEvent, l1_eightEvent }, nextEvent = new List<float>() { 5, 35, 35, 35, 30, 25, 20, 15}, timer = 240f };
        public static Level secondLevel = new Level() { events = new List<Evento>() { l2_firstEvent, l2_secondEvent, l2_thirdEvent, l2_fourthEvent, l1_thirdEvent, l2_fifthEvent, l2_sixthEvent, l1_fifthEvent, l2_seventhEvent, l1_sixthEvent, l2_eightEvent, l1_eightEvent, l2_ninethEvent, l2_tenthEvent}, nextEvent = new List<float>() { 5, 30, 30, 25, 25, 25, 20, 10, 10, 10, 5, 5, 5, 5 }, timer = 240f };

}
