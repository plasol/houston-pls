﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicManager : MonoBehaviour
{
    public GameManager gameManager;


    public Sprite[] conversationBg;
    public Sprite[] captainExpression;

    public Sprite[] charactersSprite;


    public Texture2D defaultCursor;
    public Texture2D pointerCursor;

    private GameObject[] logObject;
    public Image radiationBar;
    public Image muscleBar;
    public Image bloodBar;
    public Image sightBar;
    public Image psychBar;
    public Image coordinationBar;

    public Image radiationSpy;
    public Image muscleSpy;
    public Image bloodSpy;
    public Image sightSpy;
    public Image psychSpy;
    public Image coordinationSpy;

    public GameObject nextLevelMenu;
    public GameObject victoryMenu;
    public GameObject loseMenu;
    public Image endingScreen;

    public Sprite nextLevelBg;
    public Sprite victoryBg;
    public Sprite loseBg;

    public bool glowingLog = false;
    private Color colorTarget = Color.red;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.timer <= 30f)
        {
            var glowColor = GlowLog();
            foreach (var log in logObject)
            {
                log.transform.GetChild(0).gameObject.GetComponent<Text>().color = glowColor;
            }
        }
        else if (glowingLog)
        {
            logObject[0].transform.GetChild(0).gameObject.GetComponent<Text>().color = GlowLog();
        }
    }

    public void showDefault()
    {
        //gameManager.parameters.text = 
        gameManager.conversationText.text = "";

    }

    public void setLogObj(GameObject[] goArray)
    {
        logObject = goArray;
    }

    public void ShowLog(List<Evento> eventList)
    {
        for (int i=0; i < logObject.Length; i++)
        {
            logObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().color = new Color(0.1647f, 0.7490f, 0.2549f);

            if (eventList.Count > i) logObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = ">> " + eventList[i].logText;
            else logObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = "";
        }

        if (logObject.Length <= eventList.Count) glowingLog = true;
        else glowingLog = false;
    }

    private Color GlowLog()
    {
        if (logObject[0].transform.GetChild(0).gameObject.GetComponent<Text>().color == Color.red) colorTarget = new Color(0.1647f, 0.7490f, 0.2549f);
        else if (logObject[0].transform.GetChild(0).gameObject.GetComponent<Text>().color == new Color(0.1647f, 0.7490f, 0.2549f)) colorTarget = Color.red;
        return Color.Lerp(logObject[0].transform.GetChild(0).gameObject.GetComponent<Text>().color, colorTarget, 0.2f);
    }

    public void ShowParameters(GameState gameState)
    {
        /*
        parameters.text = "radiation = " + gameState.radiation +
            "\n\nblood = " + gameState.blood +
            "\n\ncoordination = " + gameState.coordination +
            "\n\nmuscle = " + gameState.muscle +
            "\n\npsych = " + gameState.psych +
            "\n\nsight = " + gameState.sight;
        */

        StartCoroutine(Animazioni.BarAnimation(radiationBar, radiationBar.transform.parent.gameObject, gameState.radiation, GameManager.MAX_PARAMETER_VALUE));
        StartCoroutine(Animazioni.BarAnimation(muscleBar, muscleBar.transform.parent.gameObject, gameState.muscle, GameManager.MAX_PARAMETER_VALUE));
        StartCoroutine(Animazioni.BarAnimation(bloodBar, bloodBar.transform.parent.gameObject, gameState.blood, GameManager.MAX_PARAMETER_VALUE));
        StartCoroutine(Animazioni.BarAnimation(sightBar, sightBar.transform.parent.gameObject, gameState.sight, GameManager.MAX_PARAMETER_VALUE));
        StartCoroutine(Animazioni.BarAnimation(psychBar, psychBar.transform.parent.gameObject, gameState.psych, GameManager.MAX_PARAMETER_VALUE));
        StartCoroutine(Animazioni.BarAnimation(coordinationBar, coordinationBar.transform.parent.gameObject, gameState.coordination, GameManager.MAX_PARAMETER_VALUE));

        if (gameState.radiation <= GameManager.MAX_PARAMETER_VALUE / 4) radiationBar.color = new Color(0.75f, 0, 0);
        else if (gameState.radiation <= GameManager.MAX_PARAMETER_VALUE / 2) radiationBar.color = new Color(0.75f, 0.75f, 0);
        else radiationBar.color = new Color(0, 0.75f, 0);

        if (gameState.muscle <= GameManager.MAX_PARAMETER_VALUE / 4) muscleBar.color = new Color(0.75f, 0, 0);
        else if (gameState.muscle <= GameManager.MAX_PARAMETER_VALUE / 2) muscleBar.color = new Color(0.75f, 0.75f, 0);
        else muscleBar.color = new Color(0, 0.75f, 0);

        if (gameState.blood <= GameManager.MAX_PARAMETER_VALUE / 4) bloodBar.color = new Color(0.75f, 0, 0);
        else if (gameState.blood <= GameManager.MAX_PARAMETER_VALUE / 2) bloodBar.color = new Color(0.75f, 0.75f, 0);
        else bloodBar.color = new Color(0, 0.75f, 0);

        if (gameState.sight <= GameManager.MAX_PARAMETER_VALUE / 4) sightBar.color = new Color(0.75f, 0, 0);
        else if (gameState.sight <= GameManager.MAX_PARAMETER_VALUE / 2) sightBar.color = new Color(0.75f, 0.75f, 0);
        else sightBar.color = new Color(0, 0.75f, 0);

        if (gameState.psych <= GameManager.MAX_PARAMETER_VALUE / 4) psychBar.color = new Color(0.75f, 0, 0);
        else if (gameState.psych <= GameManager.MAX_PARAMETER_VALUE / 2) psychBar.color = new Color(0.75f, 0.75f, 0);
        else psychBar.color = new Color(0, 0.75f, 0);

        if (gameState.coordination <= GameManager.MAX_PARAMETER_VALUE / 4) coordinationBar.color = new Color(0.75f, 0, 0);
        else if (gameState.coordination <= GameManager.MAX_PARAMETER_VALUE / 2) coordinationBar.color = new Color(0.75f, 0.75f, 0);
        else coordinationBar.color = new Color(0, 0.75f, 0);

    }

    public void ShowNextLevelScreen()
    {
        endingScreen.sprite = nextLevelBg;

        nextLevelMenu.SetActive(true);
        victoryMenu.SetActive(false);
        loseMenu.SetActive(false);
    }

    public void ShowVictoryScreen()
    {
        endingScreen.sprite = victoryBg;

        nextLevelMenu.SetActive(false);
        victoryMenu.SetActive(true);
        loseMenu.SetActive(false);
    }

    public void ShowLosingScreen()
    {
        endingScreen.sprite = loseBg;

        nextLevelMenu.SetActive(false);
        victoryMenu.SetActive(false);
        loseMenu.SetActive(true);
    }

    public void LightSpy(int answerIndex)
    {
        if (gameManager.selectedEvent != null)
        {
            if (gameManager.selectedEvent.content.Count == gameManager.selectedEventTextIndex + 1)
            {
                if (gameManager.selectedEvent.answers.Length > answerIndex)
                {
                    GameState variation = gameManager.selectedEvent.answers[answerIndex].parameterVariation;
                    // spie illuminate
                    if (variation.radiation != 0) radiationSpy.color = new Color(1, 1, 1, 0.4f);
                    if (variation.muscle != 0) muscleSpy.color = new Color(1, 1, 1, 0.4f);
                    if (variation.blood != 0) bloodSpy.color = new Color(1, 1, 1, 0.4f);
                    if (variation.sight != 0) sightSpy.color = new Color(1, 1, 1, 0.4f);
                    if (variation.psych != 0) psychSpy.color = new Color(1, 1, 1, 0.4f);
                    if (variation.coordination != 0) coordinationSpy.color = new Color(1, 1, 1, 0.4f);

                    // testo colorato
                    gameManager.conversationText.text = gameManager.selectedEvent.content[gameManager.selectedEventTextIndex] + "\n";

                    if (answerIndex == 0) gameManager.conversationText.text += "\n<color=yellow> <b>A</b>. " + gameManager.selectedEvent.answers[0].content + "</color>";
                    else gameManager.conversationText.text += "\n<b>A</b>. " + gameManager.selectedEvent.answers[0].content;

                    if (answerIndex == 1) gameManager.conversationText.text += "\n<color=yellow> <b>B</b>. " + gameManager.selectedEvent.answers[1].content + "</color>";
                    else gameManager.conversationText.text += "\n<b>B</b>. " + gameManager.selectedEvent.answers[1].content;

                    if (answerIndex == 2) gameManager.conversationText.text += "\n<color=yellow> <b>C</b>. " + gameManager.selectedEvent.answers[2].content + "</color>";
                    else gameManager.conversationText.text += "\n<b>C</b>. " + gameManager.selectedEvent.answers[2].content;

                }
            }
        }
    }

    public void TurnOffSpy()
    {
        // spie spente
        radiationSpy.color = new Color(1, 1, 1, 0);
        muscleSpy.color = new Color(1, 1, 1, 0);
        bloodSpy.color = new Color(1, 1, 1, 0);
        sightSpy.color = new Color(1, 1, 1, 0);
        psychSpy.color = new Color(1, 1, 1, 0);
        coordinationSpy.color = new Color(1, 1, 1, 0);

        // testo normale
        if (gameManager.selectedEvent != null)
        {
            if (gameManager.selectedEvent.content.Count == gameManager.selectedEventTextIndex + 1)
            {
                    gameManager.conversationText.text = gameManager.selectedEvent.content[gameManager.selectedEventTextIndex] + "\n";
                    gameManager.conversationText.text += "\n<b>A</b>. " + gameManager.selectedEvent.answers[0].content;
                    gameManager.conversationText.text += "\n<b>B</b>. " + gameManager.selectedEvent.answers[1].content;
                    gameManager.conversationText.text += "\n<b>C</b>. " + gameManager.selectedEvent.answers[2].content;
            }
        }
    }

    public void setDefaultCursor()
    {
        Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
    }

    public void setPointerCursor()
    {
        Cursor.SetCursor(pointerCursor, Vector2.zero, CursorMode.Auto);
    }
}
