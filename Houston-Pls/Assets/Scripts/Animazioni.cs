﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Animazioni : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static IEnumerator BarAnimation(Image bar, GameObject background, int value, int maxValue)
    {
        //Debug.Log(bar.rectTransform.rect.width + " " + background.GetComponent<RectTransform>().rect.width + " " + value + " " + maxValue);
        //battleManager.animationRunning++;
        var bgRect = background.GetComponent<RectTransform>().rect;
        var destinationLen = new Vector2(bgRect.width, (bgRect.height * value) / maxValue);
        for (int i = 0; i < 30; i++)
        {
            bar.rectTransform.sizeDelta = Vector2.Lerp(bar.rectTransform.sizeDelta, destinationLen, 12f * Time.deltaTime);

            //hitTarget.color = Color.Lerp(hitTarget.color, transitionColor, 4.5f * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }
        bar.rectTransform.sizeDelta = destinationLen;
        //battleManager.animationRunning--;
    }
}
